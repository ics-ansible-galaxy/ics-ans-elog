import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('elog')


def test_elogd_enabled_and_started(host):
    elogd = host.service('elogd')
    assert elogd.is_enabled
    assert elogd.is_running


def test_elog_homepage(host):
    cmd = host.run('curl -Lk https://localhost')
    assert '<title>ELOG - $subject</title>' in cmd.stdout
