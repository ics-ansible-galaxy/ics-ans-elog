# ics-ans-elog

Ansible playbook to install ELOG.

ELOG is an Electronic Logbook.
See https://midas.psi.ch/elog/index.html for more information.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
